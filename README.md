This framework is to Automate the below functionalities:

           1. Register on the website.
           2. Click the Sign-in option on the dashboard page.
           3. Create an Account by entering an email address. 
           4. Enter details on the Your Personal Information and click Register
           5. Validate on the landing screen - correct name and surname is displayed
           6. Logout and Login again
           7 . Add a product to the cart
           8. Proceed to the checkout page and continue till payment completes.
           9. Validate on the payments page if the product details are correct.
Code Repository

          This framework has been checked in the GitLab account and please find the link to access the below code.
                Link:- https://gitlab.com/HarshaSenapathi/anz-test-main

Code clone
          1.Install Gitbash in your system.
          2.Navigate to the folder where you need to store the project.
          3. Right-click open gitbash here.
          4.Type command given.
                git clone "https://gitlab.com/HarshaSenapathi/anz-test-main.git"
                

Framework Structure
     
           This framework was developed on Page Object Model and web-driver based on TestNG framework. Please find the below folder structure.
            
            1. com.anz.page object package contains Java class have some functionalities.
            2. com.anz.util package contains Java class have common functionalities which are used in the entire framework.
            3. src/main resources contain test data sheets and drivers.
            4. src/test/java includes test cases.

Test data Sheet
            Please check the below test data which was used in the framework in the below path:
                 path:- \src\main resources\Testdata\TestData.xlsx
Execution of Test cases

                   To run the test case please follow below steps
                      GoTo file path "\src\test\java\com\anz\SampleTestcases\ANZSampleTestcase.xml" and right click run as testNG suite.
Reports:
   The report will generate at the below path.
                   path:- "home\\Desktop\\Result\\"
     
 Deviations:
           1.Use Cucumber and Webdriverio to automate-" I have moderate knowledge on Cucumber and WebdriverIO to complete the task in the given time frame, So
I have used Page Object Model and web driver based on the TestNG framework to develop this framework".
           2.Any execution report is fine - allure preferred--"I am not having much hands-on experience in ALLURE report and which may lead to consuming more time so I used extent reports to generate reports".
           3.Tests should run in chrome as minimum and IE as an additional browser--"Due to continues crashing of IE browser in my laptop I was unable to run test cases in IE browser and developed the code for same and executed in both chrome and Firefox browsers".

            


           
           
